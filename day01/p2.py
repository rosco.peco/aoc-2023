import sys
import re

word_convs = [
        ["one", "1"],
        ["two", "2"],
        ["three", "3"],
        ["four", "4"],
        ["five", "5"],
        ["six", "6"],
        ["seven", "7"],
        ["eight", "8"],
        ["nine", "9"]
]

def convert_digits(original):
    result = ""

    for i in range(len(original)):
        sub = original[i:]
        if re.match(r"^\d", sub):
            result += sub[0]
            continue

        for conv in word_convs:
            if re.match(f"^{conv[0]}", sub):
                result += conv[1]
                break

    return result

with open(sys.argv[1], "r") as f:
    lines = list(f)

valid = [line.strip() for line in lines if line.strip()]
digits = [convert_digits(line) for line in valid]
print(digits)
sums = [(int(digit[0] + digit[-1])) for digit in digits]
print(sums)
print(sum(sums))

