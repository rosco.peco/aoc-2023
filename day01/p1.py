import re

with open("real.in", "r") as f:
    lines = list(f)

digits = [re.sub("\D", "", line.strip()) for line in lines if line.strip()]
sums = [(int(digit[0] + digit[-1])) for digit in digits]
print(sum(sums))

