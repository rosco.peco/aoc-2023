import re

poss = []
red = 12
green = 13
blue = 14

def process_sample(sample):
    result = []
    rmatch = re.search(r"(\d+) red", sample)
    gmatch = re.search(r"(\d+) green", sample)
    bmatch = re.search(r"(\d+) blue", sample)

    if rmatch:
        red = int(rmatch.group(1))
    else:
        red = 0

    if gmatch:
        green = int(gmatch.group(1))
    else:
        green = 0

    if bmatch:
        blue = int(bmatch.group(1))
    else:
        blue = 0

    return (red, green, blue)


def process_game(line):
    match = re.search(r"Game (\d+): (.+)", line)

    if match:
        return {
            "id": int(match.group(1)),
            "samples": [process_sample(sample) for sample in match.group(2).split(";")]
        }
    else:
        return {}

for r in range(red+1):
    for g in range(green+1):
        for b in range(blue+1):
            poss.append((r, g, b))

poss = set(poss)

with open("real.in", "r") as f:
    lines = f.readlines()

lines = [line.strip() for line in lines]
games = [process_game(line) for line in lines if line]

output = 0

for game in games:
    if poss.issuperset(game["samples"]):
#        print(f"Game {game['id']} is possible")
        output += game['id']
#    else:
#        print(f"Game {game['id']} is NOT possible")

print(f"Result is {output}")
